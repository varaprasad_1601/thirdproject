from django.contrib import admin
from app.models import Student,Contact_us,Categories,Register_Table,Add_Products,Cart,Orders
admin.site.site_header = "My Website"
# Register your models here.

class StudentAdmin(admin.ModelAdmin):
    #fields = ["name","email"]
    search_fields = ["name","roll_no"]
    list_display = ["name","roll_no","email","fee","gender","address","is_registered"]
    list_editable = ["fee"]
    list_filter = ["name","gender","is_registered"]
    
class ContactAdmin(admin.ModelAdmin):
    #fields = ["name","email"]
    search_fields = ["name","contact_number"]
    list_display = ["id","name","contact_number","subject","message","added_on"]
    list_editable = ["contact_number"]
    list_filter = ["name","added_on"]
    
class CategoriesAdmin(admin.ModelAdmin):
    list_display = ["id","cat_name","cover_pic","description"]
    
class RegisterAdmin(admin.ModelAdmin):
    list_display = ["user"]
    
    
    
    
class OrdersAdmin(admin.ModelAdmin):
    search_fields = ["invoice"]



class CartAdmin(admin.ModelAdmin):
    list_display = ["id","user"]



class Add_ProductsAdmin(admin.ModelAdmin):
    list_display = ["id","product_name"]
    
admin.site.register(Student,StudentAdmin)
admin.site.register(Contact_us,ContactAdmin)
admin.site.register(Categories,CategoriesAdmin)
admin.site.register(Register_Table,RegisterAdmin)
admin.site.register(Add_Products,Add_ProductsAdmin)
admin.site.register(Cart,CartAdmin)
admin.site.register(Orders,OrdersAdmin)