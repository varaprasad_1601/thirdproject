from django import forms
from app.models import Add_Products

class Add_Products_Form(forms.ModelForm):
    class Meta:
        model = Add_Products
        #fields = "__all__"
        fields = ["product_name","product_category","product_price","sale_price","product_image","details"]