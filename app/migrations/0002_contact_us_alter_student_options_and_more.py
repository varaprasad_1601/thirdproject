# Generated by Django 4.0 on 2021-12-26 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact_us',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('mobile_no', models.IntegerField(blank=True, unique=True)),
                ('subject', models.CharField(max_length=200)),
                ('message', models.TextField()),
                ('added_on', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'FeedBack Table',
            },
        ),
        migrations.AlterModelOptions(
            name='student',
            options={'verbose_name_plural': 'Studente Data'},
        ),
        migrations.AlterField(
            model_name='student',
            name='gender',
            field=models.CharField(choices=[('m', 'Male'), ('F', 'Female')], max_length=200),
        ),
    ]
