from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Student(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    roll_no = models.IntegerField(unique=True)
    fee = models.FloatField()
    c=(
        ('m','Male'),('F','Female')
    )
    gender = models.CharField(max_length=200,choices=c)
    address = models.TextField(blank=True)
    is_registered = models.BooleanField()
    
    def __str__(self):
        return self.name+" "+str(self.roll_no)
    
    class Meta:
        verbose_name_plural = "Studente Data"
        
class Contact_us(models.Model):
    name = models.CharField(max_length=200)
    contact_number = models.IntegerField(blank=True,unique=True)
    subject = models.CharField(max_length=200)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "FeedBack Table"
        

class Categories(models.Model):
    cat_name = models.CharField(max_length=200)
    cover_pic = models.FileField(upload_to="media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.cat_name
    
    class Meta:
        verbose_name_plural = "Categories"
    
    
class Register_Table(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.IntegerField(blank=True)
    profile_picture = models.ImageField(upload_to="profile/%y/%m/%d",null=True,blank=True)
    city = models.CharField(max_length=200,null=True,blank=True)
    age = models.CharField(max_length=200,null=True,blank=True)
    gender = models.CharField(max_length=200,null=True,blank=True)
    about = models.TextField(null=True,blank=True)
    occupation = models.CharField(max_length=200,null=True,blank=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    updated_on = models.DateTimeField(auto_now=True,null=True)
    
    def __str__(self):
        return self.user.username
    
    class Meta:
        verbose_name_plural = "Register Table"
        
class Add_Products(models.Model):
    seller = models.ForeignKey(User, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=250)
    product_category = models.ForeignKey(Categories, on_delete=models.CASCADE)
    product_price = models.FloatField()
    sale_price = models.FloatField()
    product_image = models.ImageField(upload_to="products/%y/%m/%d")
    details = models.TextField()
    delete = models.BooleanField(default=False)
    
    def __str__(self):
        return self.product_name
    
    class Meta:
        verbose_name_plural = "Add Product"
        
        
class Cart(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    product = models.ForeignKey(Add_Products,on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    quantity = models.IntegerField()
    items = models.IntegerField(null=True)
    order = models.BooleanField(default=False)
    remove =  models.BooleanField(default=False)
    sale_price = models.CharField(max_length=250,null=True)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.user.username
    
    class Meta:
        verbose_name_plural = "Cart"
        
        
class Orders(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    cart_id = models.CharField(max_length=250)
    product_id = models.CharField(max_length=250)
    quantity = models.CharField(max_length=250,null=True)
    sale_price = models.CharField(max_length=250,null=True)
    invoice = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.customer.username
    
    class Meta:
        verbose_name_plural = "Orders"
    