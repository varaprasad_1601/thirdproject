from django.shortcuts import render, get_object_or_404, reverse
from app.models import Contact_us,Categories,Register_Table,Add_Products,Cart,Orders
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from app.forms import Add_Products_Form
from django.db.models import Q
from django.core.mail import EmailMessage

# Create your views here.
def home(request):
    all = Contact_us.objects.all().order_by("-id")[:5]
    cats = Categories.objects.all().order_by("cat_name")
    return render(request,'home.html',{"messages":all,"category":cats})




def about(request):
    cats = Categories.objects.all().order_by("cat_name")
    return render(request,'about.html',{"category":cats})




def contact(request):
    all = Contact_us.objects.all()
    cats = Categories.objects.all().order_by("cat_name")
    if request.method=="POST":
        nm = request.POST["name"]
        con = request.POST["contact"]
        sub = request.POST["subject"]
        msg = request.POST["message"]
        
        data = Contact_us(name=nm,contact_number=con,subject=sub,message=msg)
        data.save()
        #return HttpResponse("<h1 style='color:green'>Data Saved Successfully!!!</h1>")
        res = "Dear {} Data Saved Successfully".format(nm)
        return render(request,'contact.html',{"status":res,"messages":all})
    return render(request,'contact.html',{"messages":all,"category":cats})





def register(request):
    cats = Categories.objects.all().order_by("cat_name")
    if request.method == "POST":
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        un = request.POST["uname"]
        pd = request.POST["password"]
        em = request.POST["email"]
        cn = request.POST["contact"]
        ut = request.POST["utype"]
        
        usr = User.objects.create_user(un,em,pd)
        usr.first_name = fn
        usr.last_name = ln
        if ut == "sell":
            usr.is_staff = True
        usr.save()
        
        res = Register_Table(user=usr,contact_number=cn)
        res.save()
        
        return render(request,"register.html",{"status":"Mr/Mrs. {} Your Account Created Successfully".format(un)})
        
    return render(request,"register.html",{"category":cats})





def check_user(request):
    if request.method == "GET":
        un = request.GET["usern"]
        res = User.objects.filter(username=un)
        if len(res)==1:
            return HttpResponse("exists")
        else:
            return HttpResponse("not exists")


def user_login(request):
    if request.method == "POST":
        un = request.POST["username"]
        pd = request.POST["password"]
        
        user = authenticate(username=un,password=pd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                return HttpResponseRedirect("/dashboard")
        else:
            return render(request,"home.html",{"status":"invalid username or password"})
    return HttpResponseRedirect("/user_login/")

@login_required
def seller_dashboard(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    return render(request,"seller_dashboard.html",context)

@login_required
def dashboard(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    return render(request,"customer_dashboard.html",context)

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


@login_required
def edit_profile(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    
    if request.method == "POST":
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        cn = request.POST["contact"]
        ag = request.POST["age"]
        ct = request.POST["city"]
        gd = request.POST["gender"]
        oc = request.POST["occupation"]
        ab = request.POST["about"]
        
        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()
        
        
        data.contact_number = cn
        data.age = ag
        data.city = ct
        data.gender = gd
        data.occupation = oc
        data.about = ab
        data.save()
        
        if "profile" in request.FILES:
            img = request.FILES["profile"]
            data.profile_picture = img
            data.save()

        context["statu"] = "Changes Saved Successfully" 
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    return render(request,"edit_profile.html",context)

@login_required
def change_password(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method == "POST":
        current = request.POST["currentpassword"]
        new = request.POST["newpassword"]
        
        user =  User.objects.get(id=request.user.id)
        check = user.check_password(current)
        un = user.username
        if check == True:
            user.set_password(new)
            user.save()
            user = User.objects.get(username=un)
            login(request,user)
            context["msg"] = "Password Changed Successfully"
            context["color"] = "alert-success"
        else:
            context["msg"] = "Invalid Current Password"
            context["color"] = "alert-danger"
            
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    return render(request,"change_password.html",context)

@login_required
def add_product(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
    form = Add_Products_Form()
    if request.method == "POST":
        forms = Add_Products_Form(request.POST,request.FILES)
        if forms.is_valid():
            data = forms.save(commit=False)
            login_user = User.objects.get(username=request.user.username)
            data.seller = login_user
            data.save()
            
            context["mess"] = "{} Added Successfully".format(data.product_name)
    context["form"] = form
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    return render(request,"add_product.html",context)

@login_required
def my_products(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
        
    all = Add_Products.objects.filter(seller__id=request.user.id,delete=False)
    context["products"] = all
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    return render(request,"my_products.html",context)




def product_details(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    id = request.GET["pid"]
    product = get_object_or_404(Add_Products,id=id)
    context["product"] = product
    return render(request,"product_details.html",context)

def all_products(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    products = Add_Products.objects.all().order_by("product_name")
    c = 0
    for i in products:
        if i.delete:
            pass
        else:
            c=c+1
        context["count"] = c
    context["products"] = products
    
    if "search" in request.GET:
        sch = request.GET["search"]
        products = Add_Products.objects.filter(Q(product_name__icontains=sch) | Q(product_category__cat_name__icontains=sch) | Q(details__icontains=sch))
        c = 0
        for i in products:
            if i.delete:
                pass
            else:
                c=c+1
        context["count"] = c
        context["products"] = products
        context["abcd"] = "search"
        
    if "cat" in request.GET:
        cat = request.GET["cat"]
        products = Add_Products.objects.filter(product_category__id=cat)
        c = 0
        for i in products:
            if i.delete:
                pass
            else:
                c=c+1
        context["count"] = c
        context["products"] = products
    return render(request,"all_products.html",context)

@login_required
def update_product(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    data = Register_Table.objects.get(user__id=request.user.id)
    context["data"] = data
    
    id = request.GET["pid"]
    product = get_object_or_404(Add_Products,id=id)
    context["product"] = product
    context["pid"]=id
    
    if request.method == "POST":
        pn = request.POST["pname"]
        pi = request.POST["pcategory"]
        pp = request.POST["pp"]
        sp = request.POST["sp"]
        dt = request.POST["pdetails"]
        
        pcategory = Categories.objects.get(id=pi)
        
        product.product_name = pn
        product.product_category = pcategory
        product.product_price = pp
        product.sale_price = sp
        product.details = dt
        if "pimg" in request.FILES:
            pimg = request.FILES["pimg"]
            product.product_image = pimg
        product.save()
        context["msg"] = "Changes Saved Successfully"
        
    return render(request,"update_product.html",context)

@login_required
def delete_product(request):
    context = {}
    if "pid" in request.GET:
        id = request.GET["pid"]
        product = get_object_or_404(Add_Products,id=id)
        context["product"] = product
        
        if "action" in request.GET:
            product.delete = True
            product.save()
            context["msg"] = str(product.product_name)+" Removed Successfully"
            return render(request,"my_products.html",context)
    return HttpResponseRedirect("/delete_product/")

@login_required
def send_mail(request):
    context = {}
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
        
    if request.method == "POST":
        to = request.POST["to"].split(",")
        sub = request.POST["sub"]
        msg = request.POST["msg"]
        try:
            mail = EmailMessage(sub,msg,to=to)
            mail.send()
            context["msg"] = "Email Sent"
            context["color"] = "alert-success"
            
        except:
            context["msg"] = "Could not send, Please check Internet Connection / Email Address"
            context["color"] = "alert-danger"
        
    return render(request,"send_mail.html",context)



def forgot_password(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    if request.method == "POST":
        usr = request.POST["username"]
        newpwd = request.POST["newpassword"]
        
        user = get_object_or_404(User,username=usr)
        user.set_password(newpwd)
        user.save()
        context["msg"] = "Password Reset Successfully"
    return render(request,"forgot_password.html",context)


import random
def reset_password(request):
    if request.method == "GET":
        un = request.GET["username"]
        try:
            user = get_object_or_404(User,username=un)
            otp = random.randint(1000,9999)
            msg = "Dear {}\n{} is your One Time Password (OTP)\nDo not share it with others\n\n\nThanks&Regards\nMy Website".format(user.first_name,otp)
            try:
                mail = EmailMessage("Account Verification",msg,to=[user.email])
                mail.send()
                return JsonResponse({"result":"sent","email":user.email,"realotp":otp})
            except:
                return JsonResponse({"result":"error","email":user.email})
        except:
            return JsonResponse({"result":"faild"})
    return HttpResponse("hello")


def mycart(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    items = Cart.objects.filter(user__id=request.user.id,status=False,remove=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method == "POST":
            pid = request.POST["pid"]
            quantity = request.POST["quantity"]
            is_exists = Cart.objects.filter(product__id=pid,user__id=request.user.id,status=False,remove=False)
            if len(is_exists)>0:
                context["mess"] = "Item Already Added in Your Cart"
                context["color"] = "alert-warning"
            else:
                product = get_object_or_404(Add_Products,id=pid)
                usr = get_object_or_404(User,id=request.user.id)
                cart = Cart(user=usr,product=product,quantity=quantity)
                cart.save()
                context["mess"] = "{} Added in Your Cart".format(product.product_name)
                context["color"] = "alert-success"
    else:
        context["sts"] = "Please Login First To View Your Cart"
    return render(request,"mycart.html",context)


def cart_details(request):
    items = Cart.objects.filter(user__id=request.user.id,status=False)
    pprice,sprice,quantity=0,0,0
    for i in items:
        quan = i.quantity
        pprice += float(i.product.product_price)*quan
        sprice += float(i.product.sale_price)*quan
        quantity += i.quantity
        
        
    res = {
        "pprice":pprice,"sprice":sprice,"quantity":quantity
    }
    return JsonResponse(res)


def update_quantity(request):
    if "quantity" in request.GET:
        cid = request.GET["cid"]
        quantity = request.GET["quantity"]
        cart = Cart.objects.get(id=cid)
        cart.quantity = quantity
        cart.save()
        return HttpResponse(cart.quantity)
    
    if "delete" in request.GET:
        id = request.GET["delete"]
        cart = get_object_or_404(Cart,id=id)
        if cart.order == True:
            cart.remove=True
            cart.save()
        else:
            cart.delete()
        return HttpResponse(1)




from django.conf import settings
from paypal.standard.forms import PayPalPaymentsForm
@login_required
def process_payment(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    items = Cart.objects.filter(user__id=request.user.id,status=False,remove=False)
    products = ""
    amount = 0
    quantity = 0
    cart_id = ""
    p_id = ""
    
    
    cust = User.objects.get(username=request.user.username)
    ord = Orders(customer=cust)
    ord.save()
    inv = "INV-000"+str(ord.id)
    
     
    for j in items:
        p_id += str(j.product.id)+","
        products += str(j.product.product_name)+"\n"
        amount += float(j.product.sale_price) * (j.quantity)
        quantity += j.quantity
        #inv += str(j.product.id)
        cart_id += str(j.id)+","
        j.sale_price = j.product.sale_price
        j.items = j.quantity
        j.order = True
        j.save()
        
    
        
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': amount,
        'item_name': products,
        'invoice': inv,
        'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                              reverse('payment_cancelled')),
    }
    ord.invoice = inv
    ord.quantity = quantity
    ord.sale_price = amount
    ord.cart_id = cart_id
    ord.product_id = p_id
    ord.save()
    request.session["order_id"] = ord.id


    form = PayPalPaymentsForm(initial=paypal_dict)
    context["form"] = form
    return render(request, 'process_payment.html',context)


@login_required
def payment_done(request):
    if "order_id" in request.session:
        id = request.session["order_id"]
        order_obj = get_object_or_404(Orders,id=id)
        order_obj.status = True
        order_obj.save()
        
        
        for i in order_obj.cart_id.split(",")[:-1]:
            cart_obj = get_object_or_404(Cart,id=i)
            cart_obj.status = True
            cart_obj.save()
    
    return render(request,"payment_done.html")

def payment_cancelled(request):
    return render(request,"payment_cancelled.html")




@login_required
def order_history(request):
    context = {}
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
        
    orders = Orders.objects.filter(customer__id=request.user.id).order_by("-id")
    all_orders = []
    for order in orders:
        products = []
        for id in order.cart_id.split(",")[:-1]:
            cart = get_object_or_404(Cart,id=id)
            products.append(cart)
                    
        ord = {
            "order_id":order.id,
            "products":products,
            "invoice":order.invoice,
            "quantity":order.quantity,
            "sale_price":order.sale_price,
            "status":order.status,
            "date":order.processed_on,
        }   

        all_orders.append(ord)
        
    context["my_orders"] = all_orders
    return render(request,"order_history.html",context)


@login_required
def my_customers(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
        
        products = Cart.objects.filter(product__seller__id=request.user.id,status=True)
        customers = []
        ids = []
        context["total"] = len(products)
        for i in products:
            details = {
                "id":i.user.id,
                "username":i,
                "first_name":i.user.first_name,
                "last_name":i.user.last_name,
                "email":i.user.email,
                "joined":i.user.date_joined,
            }
            profile = Register_Table.objects.get(user__id=i.user.id)
            details["contact"] = profile.contact_number
            details["profile_picture"] = profile.profile_picture
            details["gender"] = profile.gender
            
            ids.append(i.user.id)
            count = ids.count(i.user.id)
            if count<2:
                customers.append(details)
                
        context["all_customers"] = customers 
            
    return render(request,"my_customers.html",context)

def customer_details(request):
    context = {}
    cats = Categories.objects.all().order_by("cat_name")
    context["category"] = cats
    check = Register_Table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = Register_Table.objects.get(user__id=request.user.id)
        context["data"] = data
    if "uid" in request.GET:
        id = request.GET["uid"]
    products = Cart.objects.filter(user__id=id,product__seller__id=request.user.id,status=True).order_by("-id")
    all_products = []
    for i in products:
        product = {
            "id":i.product.id,
            "product_image":i.product.product_image,
            "product_name":i.product.product_name,
            "product_price":i.product.product_price,
            "sale_price":i.sale_price,
            "quantity":i.quantity,
            "date":i.updated_on
        }
        dis = round(100-((i.product.sale_price/i.product.product_price)*100))
        product["discount"] = dis
        
        productid=i.id
        order = Orders.objects.filter(customer__id=id,status=True)
        for j in order:
            for k in j.cart_id.split(",")[:-1]:
                if str(productid)==k:
                    product["invoice"] = j.invoice
                    product["order_id"] = j.id
        
        all_products.append(product)
        context["products"] = all_products
        
    
    return render(request,"customer_details.html",context)